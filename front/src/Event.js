import React from 'react';
import { Table } from 'antd';
import { withRouter } from 'react-router';
import { Form, Input, Radio, Button, Divider, Typography } from 'antd';
import { eventDetailSample, candidatesSample } from "./sampleData";
const { Title, Paragraph, Text } = Typography;

class Event extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            participant: {
                name: '',
                candidates: []
            },
            eventDetail: {
                eventId: this.props.match.params.id,
                eventName: '',
                description: '',
                dataSource: [],
                columns: []
            }
        }
    }

    componentDidMount() {
        this.fetchEventDetail();
    }

    fetchEventDetail = () => {
        const eventDetail = eventDetailSample; // これはダミーデータです

        // ダミーデータを使うのをやめて、サーバから情報を取得しましょう。
        // URL例： /api/events/1
        // 取得したデータはeventDetailに設定します。

        // サーバから取得したデータのcolumnsプロパティを元に、candidatesのデータを作りましょう
        const candidates = candidatesSample;   // これはダミーデータです

        // columnsプロパティのデータを変換する処理をここに書きましょう。
        // 処理結果をcandidatesへの代入するのも忘れずに...
        eventDetail.columns.filter(candidate => { return true })  // これはダミー処理です。何のフィルタ処理も行っていません。正しい処理を書いてください。
            .map(candidate => {
                return candidate; // これはダミー処理です。何の変換処理も行っていません。正しい処理を書いてください。
            });

        this.setState({
            eventDetail,
            participant: {
                ...this.state.participant,
                candidates
            }
        });
    };


    onChangeCandidate = (id, value) => {
        const participant = this.state.participant;
        const newCandidates = participant.candidates.map(candidate => {
            return (candidate.id === id) ?
                { ...candidate, answer: value } :
                candidate;
        });
        this.setState({
            participant: {
                ...participant,
                candidates: newCandidates
            }
        });
    };


    onChangeName = (evt) => {
        const participant = this.state.participant;
        this.setState({
            participant: {
                ...participant,
                name: evt.target.value
            }
        });
    }

    initializeParticipant = () => {
        this.setState({
            participant: {
                name: '',
                candidates: []
            }
        });
    }


    onSubmit = (evt) => {
        evt.preventDefault();
        const participant = this.state.participant;
        const votes = participant.candidates.map(candidate => ({
            candidateId: candidate.id,
            answer: candidate.answer
        }));
        const data = {
            participantName: participant.name,
            voteForms: votes
        };
        console.log(data);
        fetch(`/api/events/${this.props.match.params.id}`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(res => {
            this.initializeParticipant();
            this.fetchEventDetail();
        });
    }

    render() {
        const eventDetail = this.state.eventDetail;
        const participant = this.state.participant;

        // いっこめの<div>のしたに、イベント名、説明、出欠表を作ります。
        // eventDetailのデータを使ってください
        return (
            <div>




                <Divider />

                <div>
                    <Title level={3}>出欠を入力する</Title>
                    <Form onSubmit={this.onSubmit}>
                        <Form.Item label='名前'>
                            <Input value={participant.name} placeholder='田中' onChange={this.onChangeName} />
                        </Form.Item>

                        <Form.Item label='候補日程'>
                            {participant.candidates.map(candidate => (
                                <div key={"candidate-" + candidate.id}>
                                    <Text>{candidate.dateTime}</Text>
                                    <Radio.Group
                                        value={candidate.answer}
                                        buttonStyle="solid"
                                        onChange={(evt) => this.onChangeCandidate(candidate.id, evt.target.value)}>
                                        <Radio.Button value="〇">〇</Radio.Button>
                                        <Radio.Button value="△">△</Radio.Button>
                                        <Radio.Button value="×">×</Radio.Button>
                                    </Radio.Group>
                                    <br />
                                </div>
                            ))}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">登録する</Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        );
    }
}

export default withRouter(Event);
