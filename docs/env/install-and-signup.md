# インストールとサインアップ

ハンズオン開催までにお願いしたい事前準備がいくつかあります(ソフトウェアのインストール、サービスへのサインアップ)。

すでに開発をされている方はほとんど手間がかからないと思います。
まだの方はこの機会にがんばって環境を整えてみてください。

各プロダクトのインストール手順は示しませんが、公式ドキュメントに従ってインストールしていただければOKです。
どうしてもわからない場合は当日サポートしますが、ハンズオンの時間を有効活用するため、できるだけ準備をお願いします。

## Git

[Git](https://git-scm.com/)をインストールしておいてください。


## Node.js

[Node.js](https://nodejs.org/ja/)のVersion10(LTS)を利用できるようにしておいてください。

インストール方法はインストーラーでもnvm,nodebrew等でも構いません。

``` sh
$ node --version
v10.15.3
```

## JDK

JDK 11をインストールしておいてください。
特にこだわりがなければ、[AdoptOpenJDK](https://adoptopenjdk.net/)の`OpenJDK 11 (LTS)` / `HotSpot` をおすすめします。

``` sh
$ java -version
openjdk version "11.0.2" 2019-01-15
OpenJDK Runtime Environment 18.9 (build 11.0.2+9)
OpenJDK 64-Bit Server VM 18.9 (build 11.0.2+9, mixed mode)
```


## PostgreSQL(Docker)

データベースにPostgreSQLを使用します。ローカルPCでの開発用にPostgreSQLを準備します。
[Docker Desktop](https://www.docker.com/products/docker-desktop) (Docker for Windows, Docker for Mac)が使用できる方は、docker-compose.ymlを用意しておきますので、PostgreSQLのインストールは不要です。

Windows10 Home EditionではDocker for Windowsが使用できませんので、別途PostgreSQLの準備をお願いします。
[PostgreSQL Portable](https://sourceforge.net/projects/postgresqlportable/)
を使用するのが簡単です。

以下の情報で接続できるように設定をお願いします。

| 項目            | 値                                          |
|-----------------|---------------------------------------------|
| ホスト          | `localhost`                                 |
| ポート          | `5432`                                      |
| ユーザ          | `postgres`                                  |
| パスワード      | `password`                                  |
| (参考:JDBC URL) | `jdbc:postgresql://localhost:5432/postgres` |


## テキストエディタ

JavaScriptのコーディング用にテキストエディタを用意しておいてください。
Vim, Emacs, Visual Studio Code等、普段使い慣れているもので結構です。

特にこだわりがなければ、[Visual Studio Code](https://azure.microsoft.com/ja-jp/products/visual-studio-code/)をおすすめします。


## クラウド(Heroku)

Herokuのアカウントを作成しておいてください。
ハンズオンで作成したアプリをデプロイします。

https://signup.heroku.com/jp

[無料で使用できる範囲](https://jp.heroku.com/free)で使用します。クレジットカードの登録等は不要です。


Heroku CLIをインストールしておいてください。

https://devcenter.heroku.com/articles/heroku-cli


``` sh
$ heroku --version
heroku/7.22.7 darwin-x64 node-v11.10.1
```

CLIからHerokuにログインできればOKです。
ブラウザが起動されるので、ブラウザ上でログインを行ってください。

``` sh
$ heroku login
heroku: Press any key to open up the browser to login or q to exit: 
Opening browser to https://cli-auth.heroku.com/auth/browser/xxxxxx
Logging in... done
Logged in as hogehoge@example.com
```

## SCM, CD/CI(GitLab)

[GitLab](https://about.gitlab.com/)のアカウントを作成しておいてください。

Google,Twitter,GitHub等のアカウントでサインインすることも可能です。


## あったほうがいいもの(任意)

今回のハンズオンで必須ではないですが、あると便利と思われるものを挙げておきます。
余力のある方、興味のある方はチェックしてください。


### Java IDE

今回はバックエンドは開発しませんが、コード等を見たい場合にあると便利です。
Java用のIDEを用意しておくことをおすすめします。

特にこだわりがなければ、以下の2つがおすすめです。

- [IntelliJ](https://www.jetbrains.com/idea/)
- [Eclipse](https://www.eclipse.org/)

IntelliJは、無償で入手できるCommunity Editionで大丈夫です。

アプリケーションのビルドや実行には[Maven](https://maven.apache.org/)を使用しますが、
[Maven Wrapper](https://github.com/takari/maven-wrapper)を用意しておきますので、
Mavenの事前準備は不要です。

### Chrome

開発には任意のブラウザを使用いただけますが、
[Chrome](https://www.google.com/intl/ja_ALL/chrome/)があると、トラブルシューティングやデバッグ等に便利です。

Visual Studio Codeをお使いの場合は、[Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)を
インストールすると簡単にVSCode上でデバッガが使用できるのでおすすめです。
