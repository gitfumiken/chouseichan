# Herokuへデプロイ

一通り動作するアプリケーションができたら、ローカルPCからHerokuにデプロイし、
インターネットに公開してみましょう。

## heroku login

Herokuにログインします。

```sh
heroku login
```

## heroku create

Herokuに新規アプリケーションを登録します。

``` bash
heroku create <アプリケーション名>
```

アプリケーション名は任意ですが、既存のアプリケーションと重複はできないので
被りにくそうな名前にしておくのが無難です。

アプリケーション名はサブドメインに使用されます。

【例】
https://chouseisan.herokuapp.com


## アプリケーション名を設定(pom.xml)に反映

今回は[Heroku Maven Plugin](https://devcenter.heroku.com/articles/deploying-java-applications-with-the-heroku-maven-plugin)を使用しているので、プラグインにアプリケーション名を設定します。

pom.xmlの以下の箇所を書き換えます。

``` xml
  <properties>
    <!-- 中略 -->
    
    <heroku.appName>chouseichan</heroku.appName>　<!-- ここの値を自分のアプリケーション名に変更 -->
  </properties>
```

## PostgreSQLデータベースの作成

https://dashboard.heroku.com/apps/<アプリケーション名>
にブラウザでアクセスします。

以下のように、`Installed add-ons`に`Heroku Postgres`が設定済みであればOKです。

![ダッシュボード](./img/heroku_dashboard.png)

何も表示されていない場合は、以下の操作を行いPostgreSQLを追加します。


- `Configure Add-ons`をクリックする
- `Add-ons`に`postgres`と入力する
- 候補に`Heroku Postgres`が表示されるので、選択、追加する

![addon](./img/heroku_add_on.png)

## アプリケーションのビルドとデプロイ

``` bash
./mvnw -P production clean heroku:deploy
```

コマンドが正常終了し、Herokuのコンソール(`https://dashboard.heroku.com/apps/<アプリケーション名>/activity`)で`Build Succeeded`と表示されていればビルド、デプロイは成功です。


![activity](./img/heroku_activity.png)

実際にPCやスマートフォン等でアプリケーションにアクセスしてみましょう。

開発環境と同じように動作すればOKです。  
[GitLab_CICDを設定](./04_GitLab_CICDを設定.md)に進んでください。

### アプリケーション実行時にエラーが発生した場合

ビルド、デプロイが成功しても、アプリケーション実行時にエラーが発生する場合があります。
この場合、以下のコマンドでログが確認できます。

``` bash
heroku logs --app <アプリケーション名>
```

