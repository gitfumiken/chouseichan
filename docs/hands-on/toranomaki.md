# 虎の巻

## Visual Studio CodeとChromeでデバッグ

デバッガを使ったデバッグができます。


### プラグインをインストールする

- [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)

### VSCodeからデバッグ開始

`npm start`した状態でVSCodeとChromeでデバッグします。

VSCode左側の`デバッグ`アイコンをクリックし、`デバッグの開始`ボタンを押すと、Chromeが起動します。

![デバッガ起動](./img/デバッガ起動.png)


### ブレークポイントの設定

ソースコード行番号左側にブレークポイントが指定できます。


### アプリケーションの実行

アプリケーションが動作してブレークポイントに到達すると、処理が停止し、
変数を見る、デバッグコンソールで任意の命令を実行する等の操作ができるようになります。

![ステップ実行](./img/ステップ実行.png)


うまくブレークしない場合は、`npm start`を実行し直す、デバッガ起動をやり直す等すると、
期待通りの動作となることがあります。

## React Developer Tools

Reactコンポーネントの状態(DOM, props, state)を、Chromeのデベロッパーツールから確認できます。


### プラグインをインストールする

- [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/)


### ツールを起動する

ChromeでReactアプリケーションを表示した状態で、Chromeデベロッパーツールを起動します。
`Network`, `Performance`の右隣りに`React`タブがあります。
ここでコンポーネントの状態を確認できます。


![react_devtools.png](./img/react_devtools.png)

## Ant Design

https://ant.design/

- [ボタン](https://ant.design/components/button/)
- [タイトルや文字](https://ant.design/components/typography/)
- [入力フォーム](https://ant.design/components/form/)
- [テキストフィールド](https://ant.design/components/input/)
- [ラジオ](https://ant.design/components/radio/)

## React Router

https://reacttraining.com/react-router/web/guides/quick-start

- [基本的なサンプル](https://reacttraining.com/react-router/web/example/basic)
- [URLからパラメータ取得](https://reacttraining.com/react-router/web/example/url-params)
- [JSからAPIを使う](https://reacttraining.com/react-router/web/api/withRouter)


