package jp.co.tis.tiw.chouseichan.dao;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;

import jp.co.tis.tiw.chouseichan.dto.EventDetailDto;
import jp.co.tis.tiw.chouseichan.entity.Event;

import java.util.List;

/**
 * イベントDAO
 */
@ConfigAutowireable
@Dao
public interface EventDao {

    /**
     * イベントを挿入する。
     * @param event イベント
     * @return 挿入件数
     */
    @Insert
    int insert(Event event);

    /**
     * イベントIDを指定してイベントを取得する。
     * @param eventId イベントID
     * @return イベント詳細
     */
    @Select
    List<EventDetailDto> selectById(Integer eventId);
}
