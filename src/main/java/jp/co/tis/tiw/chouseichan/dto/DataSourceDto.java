package jp.co.tis.tiw.chouseichan.dto;

import java.util.List;

import jp.co.tis.tiw.chouseichan.entity.Vote;

public class DataSourceDto {

    private String name;
    private List<Vote> votes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }
}
