package jp.co.tis.tiw.chouseichan.controller;

import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import jp.co.tis.tiw.chouseichan.controller.EventController;
import jp.co.tis.tiw.chouseichan.dto.DataSourceDto;
import jp.co.tis.tiw.chouseichan.dto.EventDisplayDto;
import jp.co.tis.tiw.chouseichan.entity.Candidate;
import jp.co.tis.tiw.chouseichan.entity.Vote;
import jp.co.tis.tiw.chouseichan.service.EventService;

class EventControllerTest {

    private MockMvc mvc;

    private EventService service;

    @BeforeEach
    void setUp() {
        service = mock(EventService.class);
        EventController controller = new EventController(service);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void getEvent() throws Exception {
        EventDisplayDto event = new EventDisplayDto();
        event.setEventId(1);
        event.setEventName("aaa");
        event.setDescription("bbb");

        Candidate candidate1 = new Candidate();
        candidate1.setCandidateId(1);
        candidate1.setDateTime("2019/3/30");
        Candidate candidate2 = new Candidate();
        candidate2.setCandidateId(2);
        candidate2.setDateTime("2019/3/31");
        event.setColumns(List.of(candidate1, candidate2));

        DataSourceDto dataSourceDto1 = new DataSourceDto();
        dataSourceDto1.setName("ccc");
        Vote vote1 = new Vote();
        vote1.setCandidateId(1);
        vote1.setAnswer("○");
        Vote vote2 = new Vote();
        vote2.setCandidateId(2);
        vote2.setAnswer("△");
        dataSourceDto1.setVotes(List.of(vote1, vote2));

        DataSourceDto dataSourceDto2 = new DataSourceDto();
        dataSourceDto2.setName("ddd");
        Vote vote3 = new Vote();
        vote3.setCandidateId(1);
        vote3.setAnswer("△");
        Vote vote4 = new Vote();
        vote4.setCandidateId(2);
        vote4.setAnswer("×");
        dataSourceDto2.setVotes(List.of(vote3, vote4));

        event.setDataSource(List.of(dataSourceDto1, dataSourceDto2));

        given(service.getEvent(1)).willReturn(event);

        mvc.perform(get("/events/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.eventId").value(1))
                .andExpect(jsonPath("$.eventName").value("aaa"))
                .andExpect(jsonPath("$.description").value("bbb"))

                .andExpect(jsonPath("$.columns[0].title").value("名前"))
                .andExpect(jsonPath("$.columns[0].dataIndex").value("name"))
                .andExpect(jsonPath("$.columns[0].key").value("name"))
                .andExpect(jsonPath("$.columns[1].title").value("2019/3/30"))
                .andExpect(jsonPath("$.columns[1].dataIndex").value("1"))
                .andExpect(jsonPath("$.columns[1].key").value("1"))
                .andExpect(jsonPath("$.columns[2].title").value("2019/3/31"))
                .andExpect(jsonPath("$.columns[2].dataIndex").value("2"))
                .andExpect(jsonPath("$.columns[2].key").value("2"))

                .andExpect(jsonPath("$.dataSource[0].key").value(1))
                .andExpect(jsonPath("$.dataSource[0].name").value("ccc"))
                .andExpect(jsonPath("$.dataSource[0].1").value("○"))
                .andExpect(jsonPath("$.dataSource[0].2").value("△"))
                .andExpect(jsonPath("$.dataSource[1].key").value(2))
                .andExpect(jsonPath("$.dataSource[1].name").value("ddd"))
                .andExpect(jsonPath("$.dataSource[1].1").value("△"))
                .andExpect(jsonPath("$.dataSource[1].2").value("×"));

        verify(service).getEvent(1);
    }

    @Test
    void getEvent_NotFound() throws Exception {
        given(service.getEvent(1)).willReturn(null);

        mvc.perform(get("/events/1"))
                .andExpect(status().isNotFound());

        verify(service).getEvent(1);
    }
}
